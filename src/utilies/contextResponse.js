const ConteRespo = (code) => {

    let response = {
        message: '',
        type: undefined
    }

    switch(code) {

        //100-200
        //unknow
        case 100:
            response.message = 'Continue. Si el error persiste, comuníquese con el Webmaster. Código: 100. '
            response.type = 4
            break;
        case 101:
            response.message = 'Switching Protocols. Si el error persiste, comuníquese con el Webmaster. Código: 101.'
            response.type = 4
            break;
        case 102:
            response.message = 'Processing. Si el error persiste, comuníquese con el Webmaster. Código: 102.'
            response.type = 4
            break;
        case 103:
            response.message = 'Early Hints. Si el error persiste, comuníquese con el Webmaster. Código 103.'
            response.type = 4
            break;
        
        //200-300
        //okay
        case 200:
            response.message = 'The request succeeded.'
            response.type = 1
            break;
        case 201:
            response.message = 'The request succeeded, and a new resource was created as a result.'
            response.type = 1
            break;

        //warning
        case 202:
            response.message = 'The request has been received but not yet acted upon. Si el inconveniente persiste, comuníquese con el Webmaster. Código 202'
            response.type = 2
            break;
        case 203:
            response.message = 'The request was successful but the enclosed payload has been modified by a transforming proxy. Warning!. Código 203'
            response.type = 2
            break;
        case 204:
            response.message = 'There is no content to send for this request, but the headers may be useful. Si el inconveniente persiste, comuníquese con el Webmaster. Código 204'
            response.type = 2
            break;
        case 205:
            response.message = 'Reset the document which sent this request. Si el inconveniente persiste, comuníquese con el Webmaster. Código 205'
            response.type = 2
            break;
        
        //unknow
        case 206:
            response.message = 'Partial Content. Por favor comuníquese con el Webmaster. Código 206'
            response.type = 4
            break;
        case 207:
            response.message = 'Multi-Status. Por favor comuníquese con el Webmaster. Código 207'
            response.type = 4
            break;
        case 208:
            response.message = 'Already Reported. Por favor comuníquese con el Webmaster. Código 208'
            response.type = 4
            break;
        case 226:
            response.message = 'IM Used. Por favor comuníquese con el Webmaster. Código 226'
            response.type = 4
            break;
        
        //300-400
        //warning
        case 300:
            response.message = 'Multiple Choices. Código 300'
            response.type = 2
            break;
        case 301:
            response.message = 'Moved Permanently. Código 301'
            response.type = 2
            break;
        case 302:
            response.message = 'Found. Código 302'
            response.type = 2
            break;
        case 303:
            response.message = 'See Other. Código 303'
            response.type = 2
            break;
        case 304:
            response.message = 'Not Modified. Código 304'
            response.type = 2
            break;
        case 307:
            response.message = 'Temporary Redirect. Código 307'
            response.type = 2
            break;
        case 308:
            response.message = 'Permanent Redirect. Código 308'
            response.type = 2
            break;

        //400-500
        //error cliente
        case 400:
            response.message ='Bad Request. The server cannot or will not process the request due to something that is perceived to be a client error. Faltal error, Por favor comuníquese con el Webmaster. Código 400'
            response.type = 3
            break;
        case 401:
            response.message ='Unauthorized. Faltal error, Por favor comuníquese con el Webmaster. Código 401.'
            response.type = 3
            break;
        case 402:
            response.message ='Payment Required. Faltal error, Por favor comuníquese con el Webmaster. Código 402.'
            response.type = 3
            break;
        case 403:
            response.message ='Forbidden. Faltal error, Por favor comuníquese con el Webmaster. Código 403.'
            response.type = 3
            break;
        case 404:
            response.message ='Not Found. Faltal error, Por favor comuníquese con el Webmaster. Código 404.'
            response.type = 3
            break;
        case 405:
            response.message ='Method Not Allowed. Faltal error, Por favor comuníquese con el Webmaster. Código: 405'
            response.type = 3
            break;
        case 406:
            response.message ='Not Acceptable. Faltal error, Por favor comuníquese con el Webmaster. Código: 406'
            response.type = 3
            break;
        case 407:
            response.message ='Proxy Authentication Required. Faltal error, Por favor comuníquese con el Webmaster. Código: 407'
            response.type = 3
            break;
        case 408:
            response.message ='Request Timeout. Faltal error, Por favor comuníquese con el Webmaster. Código: 408'
            response.type = 3
            break;
        case 409:
            response.message ='Conflict. Faltal error, Por favor comuníquese con el Webmaster. Código: 409'
            response.type = 3
            break;
        case 410:
            response.message ='Gone. Faltal error, Por favor comuníquese con el Webmaster. Código: 410'
            response.type = 3
            break;
        case 411:
            response.message ='Length Required. Faltal error, Por favor comuníquese con el Webmaster. Código: 411'
            response.type = 3
            break;
        case 412:
            response.message ='Precondition Failed. Faltal error, Por favor comuníquese con el Webmaster. Código: 412'
            response.type = 3
            break;
        case 413:
            response.message ='Payload Too Large.Faltal error, Por favor comuníquese con el Webmaster. Código: 413'
            response.type = 3
            break;
        case 414:
            response.message ='URI Too Long. Faltal error, Por favor comuníquese con el Webmaster. Código: 414'
            response.type = 3
            break;
        case 415:
            response.message ='Unsupported Media Type. Faltal error, Por favor comuníquese con el Webmaster. Código: 415'
            response.type = 3
            break;
        case 416:
            response.message ='Range Not Satisfiable. Faltal error, Por favor comuníquese con el Webmaster. Código: 416'
            response.type = 3
            break;
        case 417:
            response.message ='Expectation Failed. Faltal error, Por favor comuníquese con el Webmaster. Código: 417'
            response.type = 3
            break;
        case 418:
            response.message ='Im a teapot. Faltal error, Por favor comuníquese con el Webmaster. Código: 418'
            response.type = 3
            break;
        case 421:
            response.message ='Misdirected Request. Faltal error, Por favor comuníquese con el Webmaster. Código: 421'
            response.type = 3
            break;
        case 422:
            response.message ='Unprocessable Entity. Faltal error, Por favor comuníquese con el Webmaster. Código: 422'
            response.type = 3
            break;
        case 423:
            response.message ='Locked. Faltal error, Por favor comuníquese con el Webmaster. Código: 423'
            response.type = 3
            break;
        case 424:
            response.message ='Failed Dependency. Faltal error, Por favor comuníquese con el Webmaster. Código: 424'
            response.type = 3
            break;
        case 425:
            response.message ='Too Early. Faltal error, Por favor comuníquese con el Webmaster. Código: 425'
            response.type = 3
            break;
        case 426:
            response.message ='Upgrade Required. Faltal error, Por favor comuníquese con el Webmaster. Código: 426'
            response.type = 3
            break;
        case 428:
            response.message ='Precondition Required. Faltal error, Por favor comuníquese con el Webmaster. Código: 428'
            response.type = 3
            break;
        case 429:
            response.message ='Too Many Requests. Faltal error, Por favor comuníquese con el Webmaster. Código: 429'
            response.type = 3
            break;
        case 431:
            response.message ='Request Header Fields Too Large. Faltal error, Por favor comuníquese con el Webmaster. Código: 431'
            response.type = 3
            break;
        case 451:
            response.message ='Unavailable For Legal Reasons. Faltal error, Por favor comuníquese con el Webmaster. Código: 451'
            response.type = 3
            break;
        
        //500-
        //Error server
        case 500:
            response.message='Ha ocurrido un error interno en el servidor. Faltal error, Por favor comuníquese con el Webmaster. Código 500'
            response.type = 3
            break;
        case 501:
            response.message = 'Not Implemented. Faltal error, Por favor comuníquese con el Webmaster. Código 501'
            response.type = 3
            break;
        case 502:
            response.message = 'Bad Gateway. Faltal error, Por favor comuníquese con el Webmaster. Código 502'
            response.type = 3
            break;
        case 503:
            response.message = 'Service Unavailable. Faltal error, Por favor comuníquese con el Webmaster. Código 503'
            response.type = 3
            break;
        case 504:
            response.message = 'Gateway Timeout. Faltal error, Por favor comuníquese con el Webmaster. Código 504'
            response.type = 3
            break;
        case 505:
            response.message = 'HTTP Version Not Supported. Faltal error, Por favor comuníquese con el Webmaster. Código 505'
            response.type = 3
            break;
        case 506:
            response.message = 'Variant Also Negotiates. Faltal error, Por favor comuníquese con el Webmaster. Código 506'
            response.type = 3
            break;
        case 507:
            response.message = 'Insufficient Storage. Faltal error, Por favor comuníquese con el Webmaster. Código 507'
            response.type = 3
            break;
        case 508:
            response.message = 'Loop Detected. Faltal error, Por favor comuníquese con el Webmaster. Código 508'
            response.type = 3
            break;
        case 510:
            response.message = 'Not Extended. Faltal error, Por favor comuníquese con el Webmaster. Código 510'
            response.type = 3
            break;
        case 511:
            response.message = 'Network Authentication Required. Faltal error, Por favor comuníquese con el Webmaster. Código 511'
            response.type = 3
            break;
        default:
            response.message = 'Estado desconocido. Por favor comuníquese con el Webmaster. Código desconocido'
            response.type = 4
    }

    return response

}

export {ConteRespo}