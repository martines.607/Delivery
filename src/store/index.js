import Vue from 'vue'
import Vuex from 'vuex'
import jwt_decode from "jwt-decode";
import router from '@/router';

Vue.use(Vuex)
/* eslint-disable */
export default new Vuex.Store({
    state: {
        user: null,
        roles: [],
        refModalNotification: null,
        token: null,

        shipping: null
    },
    getters: {
        userAutenticate(state) {
            return state.user != null
        },
        userRole(state) {
            return state.roles.length > 0 ? state.roles[0] : null
        },
        getToken(state) {
            return state.token
        },
        getShipping(state) {
            return state.shipping
        },
        getUser(state) {
            return state.user
        }
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload
        },
        setRoles(state, payload) {
            state.roles = payload
        },
        setRefModalNotification(state, payload) {
            state.refModalNotification = payload
        },
        setToken(state, payload) {
            state.token = payload
        },
        setShipping(state, payload) {
            state.shipping = payload
        }
    },
    actions: {

        saveSession({commit}, payload) {
            let data = jwt_decode(payload)
            if(data != null) { 
                if( (data.sub != null) && (data.role != null) ) {
                    if(Array.isArray(data.role)) {
                        if(data.role.length > 0) {
                            localStorage.setItem('token', payload)
                            commit('setRoles', data.role)
                            commit('setUser', data.sub)
                            commit('setToken', payload)
                            switch(data.role[0]) {
                                case 'ROLE_ADMIN':
                                    router.replace({name: 'admin'}).catch((e) =>{
                                        console.log("error: ", e)
                                    })
                                    break;
                                case 'ROLE_OPERATOR':
                                    router.replace({name: 'operator'}).catch((e) =>{
                                        console.log("error: ", e)
                                    })
                                    break;
                            }
                            
                            if(data.role[0] == "ROLE_ADMIN") {
                                
                            }
                            
                        }
                    }
                }
            }            
        },

        closeSession({commit}) {
            commit('setUser', null)
            commit('setRoles', [])
            localStorage.removeItem('token')
            router.replace('/login')
        },

        async loadStorage({commit, state}) {
            let token = localStorage.getItem('token')
            if(token) {
                let data = jwt_decode(token)
                console.log("***: ", data)
                commit('setRoles', data.role)
                commit('setUser', data.sub)
                commit('setToken', token)
            } else {
                commit('setUser', null)
                commit('setRoles', [])
                commit('setToken', null)
            }
        },

        initialModalNotification({commit}, payload) {
            commit('setRefModalNotification', payload)
        },

        openModal({commit}, payload) {
            console.log("**: ", this.state.refModalNotification)
            this.state.refModalNotification.openModal(payload.title, payload.body, payload.type)
        },

        closeModal({commit}) {
            this.state.refModalNotification.closeModal()
        }

    },
    modules: {
    }
})
/* eslint-disable */
